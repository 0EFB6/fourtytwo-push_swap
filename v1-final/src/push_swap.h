/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/30 15:44:42 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/17 12:14:15 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H
# include <stdlib.h> // size_t
# include <unistd.h> // write
# define HELPFUL_ERROR_MSG 0

typedef struct s_stack
{
	int		*content;
	size_t	nbr_element;
}	t_stack;

typedef struct s_stacks
{
	int		counter;
	t_stack	*a;
	t_stack	*b;
}	t_stacks;

typedef enum e_op_type
{
	RR_RURD_SW,
	RU_SW_RRD,
	RRU_RD,
	RRR_RRURRD
}	t_op_type;

typedef struct s_operation
{
	size_t		nbr_ru;
	size_t		nbr_rd;
	size_t		nbr_rru;
	size_t		nbr_rrd;
	size_t		nbr_sw;
	size_t		nbr_operation;
	char		ru[3];
	char		rd[3];
	char		rru[4];
	char		rrd[4];
	char		sw[3];
	t_op_type	op_type;
}	t_operation;

typedef enum e_sort_type
{
	ASCENDING,
	DESCENDING
}	t_sort_type;

typedef enum e_push_type
{
	ATOB,
	BTOA
}	t_push_type;

/* [core]	 */
/* parser.c  */
t_stack		*parser(int argc, char **argv);
void		set_op_context(t_operation *context, t_push_type type);

/* sort.c */
void		sort_three_element(t_stacks *stacks);
void		sort_two_elements(t_stacks *stacks);
void		sort_stacks(t_stacks *stacks);

/* validate.c */
char		valid_stack(t_stack *stack);
char		is_sorted(const	t_stacks *stacks);

/* [operaion]		 */
/* basic_operation.c */
size_t		insert(int n, const int *arr, size_t len, t_sort_type type);
char		push(t_stack *dest, t_stack *src);
char		rotate_up(t_stack *stack);
char		rotate_down(t_stack *stack);
char		swap(t_stack *stack);

/* commit_operation.c */
void		commit_op(t_stacks *stacks, const t_operation *op,
				const t_operation *context);

/* do_op.c   */
void		do_op(t_stacks *stacks, const char *op, char printable_op);

/* operation.c */
t_operation	*get_cheapest_op(t_stack *src, t_stack *dest, t_sort_type type);

/* [utils]   */
/* array.c */
void		*resize_arr(void *arr, size_t old_size,
				size_t new_size, size_t size);
void		free_ptr_arr(void **ptr_arr);
size_t		get_nbr_ptr(void **ptr_arr);
size_t		get_element_index(int n, int *arr, size_t len);

/* atoi.c */
int			ft_atoi_safe(const char *s);

/* error.c */
void		ft_putendl_fd(char *str, int fd);
void		error(const char *msg);

/* math.c */
int			max_int(void);
int			min_int(void);
int			min(const int *arr, size_t len, size_t *i_min);
int			max(const int *arr, size_t len, size_t *i_max);
size_t		roll_index(size_t index, size_t diff, size_t min, size_t max);

/* struct.c */
t_stack		*init_stack(size_t nbr_element);
t_stacks	*init_stacks(int argc, char **argv);
void		destroy_stack(t_stack *stack);
void		destroy_stacks(t_stacks *stacks);

#endif