/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 11:35:20 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/16 17:02:37 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "../push_swap.h"

void	ft_putendl_fd(char *str, int fd)
{
	if (str)
	{
		write(fd, str, ft_strlen(str));
		write(fd, "\n", 1);
	}
}

void	error(const char *msg)
{
	if (HELPFUL_ERROR_MSG)
		ft_putendl_fd((char *)msg, STDERR_FILENO);
	else
		ft_putendl_fd("Error", STDERR_FILENO);
	exit(-1);
}
