/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_bonus.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/03 15:06:00 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/17 12:07:52 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "../push_swap.h"

static void	sanitize_operation_str(char *str)
{
	while (*str != '\0' && *str != '\n')
		str++;
	if (*str == '\n')
		*str = '\0';
}

int	main(int argc, char **argv)
{
	char		*str;
	t_stacks	*stacks;

	if (argc == 1)
		return (0);
	stacks = init_stacks(argc, argv);
	if (!valid_stack(stacks->a))
		error("[checker] Invalid stack A");
	str = get_next_line(STDIN_FILENO);
	while (str)
	{
		sanitize_operation_str(str);
		do_op(stacks, str, 0);
		free(str);
		str = get_next_line(STDIN_FILENO);
	}
	if (is_sorted(stacks))
		ft_putendl_fd("OK", STDOUT_FILENO);
	else
		ft_putendl_fd("KO", STDOUT_FILENO);
	destroy_stacks(stacks);
	return (0);
}
