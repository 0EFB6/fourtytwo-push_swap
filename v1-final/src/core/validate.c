/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 14:50:55 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/16 17:02:00 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

char	valid_stack(t_stack *stack)
{
	size_t	i;

	i = 0;
	if (stack == NULL)
		return (0);
	while (i < stack->nbr_element)
	{
		if ((size_t)get_element_index(stack->content[i],
				stack->content, stack->nbr_element) != i)
			return (0);
		i++;
	}
	return (1);
}

char	is_sorted(const	t_stacks *stacks)
{
	size_t	i;

	i = 0;
	if (stacks == NULL || stacks->a == NULL || stacks->b == NULL)
		return (-1);
	if (stacks->b->nbr_element == 0 && stacks->a->nbr_element < 2)
		return (1);
	if (stacks->b->nbr_element > 0)
		return (0);
	while (i < stacks->a->nbr_element - 1)
	{
		if (stacks->a->content[i] > stacks->a->content[i + 1])
			return (0);
		i++;
	}
	return (1);
}
