/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 14:37:29 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/17 12:13:19 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	main(int argc, char **argv)
{
	t_stacks	*stacks;

	if (argc == 1)
		return (0);
	stacks = init_stacks(argc, argv);
	if (!valid_stack(stacks->a))
		error("[push_swap] Invalid stack A");
	sort_stacks(stacks);
	destroy_stacks(stacks);
	return (0);
}
