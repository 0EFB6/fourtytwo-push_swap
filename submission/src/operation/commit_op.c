/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   commit_op.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/03 14:19:14 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/17 12:38:59 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

static void	commit_op_a(t_stacks *stacks, const t_operation *op,
				const t_operation *context);
static void	commit_op_b(t_stacks *stacks, const t_operation *op,
				const t_operation *context);
static void	commit_op_c(t_stacks *stacks, const t_operation *op,
				const t_operation *context);
static void	commit_op_d(t_stacks *stacks, const t_operation *op,
				const t_operation *context);

void	commit_op(t_stacks *stacks, const t_operation *op,
			const t_operation *context)
{
	if (op->op_type == RR_RURD_SW)
		commit_op_a(stacks, op, context);
	else if (op->op_type == RU_SW_RRD)
		commit_op_b(stacks, op, context);
	else if (op->op_type == RRU_RD)
		commit_op_c(stacks, op, context);
	else if (op->op_type == RRR_RRURRD)
		commit_op_d(stacks, op, context);
	if (context->rd[1] == 'a')
		do_op(stacks, "pa", 1);
	else
		do_op(stacks, "pb", 1);
}

static void	commit_op_a(t_stacks *stacks, const t_operation *op,
				const t_operation *context)
{
	const char	*op_type;
	size_t		performed;
	size_t		target;

	op_type = "rr";
	performed = -1;
	target = op->nbr_rd;
	if (op->nbr_ru < op->nbr_rd)
		target = op->nbr_ru;
	while (++performed < target)
		do_op(stacks, op_type, 1);
	target = op->nbr_operation - op->nbr_sw;
	if (op->nbr_ru > op->nbr_rd)
		op_type = context->ru;
	else
		op_type = context->rd;
	while (performed++ < target)
		do_op(stacks, op_type, 1);
	op_type = context->sw;
	if (op->nbr_sw > 0)
		do_op(stacks, op_type, 1);
}

static void	commit_op_b(t_stacks *stacks, const t_operation *op,
				const t_operation *context)
{
	const char	*op_type;
	size_t		performed;
	size_t		target;

	op_type = context->ru;
	performed = 0;
	target = op->nbr_ru;
	while (performed++ < target)
		do_op(stacks, op_type, 1);
	op_type = context->sw;
	if (op->nbr_sw > 0)
		do_op(stacks, op_type, 1);
	op_type = context->rrd;
	performed = 0;
	target = op->nbr_rrd;
	while (performed++ < target)
		do_op(stacks, op_type, 1);
}

static void	commit_op_c(t_stacks *stacks, const t_operation *op,
				const t_operation *context)
{
	const char	*op_type;
	size_t		performed;
	size_t		target;

	op_type = context->rru;
	performed = -1;
	target = op->nbr_rru;
	while (++performed < target)
		do_op(stacks, op_type, 1);
	op_type = context->rd;
	target = op->nbr_operation;
	while (performed++ < target)
		do_op(stacks, op_type, 1);
}

static void	commit_op_d(t_stacks *stacks, const t_operation *op,
				const t_operation *context)
{
	const char	*op_type;
	size_t		performed;
	size_t		target;

	op_type = "rrr";
	performed = -1;
	target = op->nbr_rrd;
	if (op->nbr_rru < op->nbr_rrd)
		target = op-> nbr_rru;
	while (++performed < target)
		do_op(stacks, op_type, 1);
	if (op->nbr_rru > op->nbr_rrd)
		op_type = context->rru;
	else
		op_type = context->rrd;
	target = op->nbr_operation;
	while (performed++ < target)
		do_op(stacks, op_type, 1);
}
