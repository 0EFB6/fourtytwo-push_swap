/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/03 13:06:39 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/17 12:20:06 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

static void	set_nbr_swap(t_operation *swap_op, t_stack *src, size_t i_src,
	t_sort_type type)
{
	int	previous_element;
	int	element_to_push;

	swap_op->nbr_sw = 0;
	if ((swap_op->op_type != RR_RURD_SW && swap_op->op_type != RU_SW_RRD)
		|| (swap_op->op_type == RR_RURD_SW
			&& swap_op->nbr_rd >= swap_op->nbr_ru)
		|| (swap_op->op_type == RU_SW_RRD && swap_op->nbr_ru == 0))
		return ;
	element_to_push = src->content[i_src];
	if (i_src > 0)
		previous_element = src->content[i_src];
	else
		previous_element = src->content[src->nbr_element - 1];
	if ((type == DESCENDING && element_to_push < previous_element)
		|| (type == ASCENDING && element_to_push > previous_element))
	{
		swap_op->nbr_sw++;
		swap_op->nbr_ru--;
	}
}

static void	set_plan(t_operation *swap_op, size_t nbr_operation,
	t_op_type op_type)
{
	swap_op->nbr_operation = nbr_operation;
	swap_op->op_type = op_type;
}

static void	get_optimum_plan(t_operation *swap_op)
{
	size_t	nbr_op;

	swap_op->nbr_operation = swap_op->nbr_ru;
	if (swap_op->nbr_rd > swap_op->nbr_operation)
		swap_op->nbr_operation = swap_op->nbr_rd;
	swap_op->op_type = RR_RURD_SW;
	nbr_op = swap_op->nbr_rru + swap_op->nbr_rd;
	if (nbr_op < swap_op->nbr_operation)
		set_plan(swap_op, nbr_op, RRU_RD);
	nbr_op = swap_op->nbr_ru + swap_op->nbr_rrd;
	if (nbr_op < swap_op->nbr_operation)
		set_plan(swap_op, nbr_op, RU_SW_RRD);
	nbr_op = swap_op->nbr_rru;
	if (nbr_op < swap_op->nbr_rrd)
		nbr_op = swap_op->nbr_rrd;
	if (nbr_op < swap_op->nbr_operation)
		set_plan(swap_op, nbr_op, RRR_RRURRD);
}

static t_operation	*calculate_optimum_plan(t_stack *src, size_t i_src,
				t_stack *dest, t_sort_type type)
{
	t_operation	*swap_op;

	swap_op = (t_operation *)malloc(sizeof(t_operation));
	if (swap_op == NULL)
		return (NULL);
	swap_op->nbr_ru = i_src;
	swap_op->nbr_rd = insert(src->content[i_src], dest->content,
			dest->nbr_element, type);
	swap_op->nbr_rru = src->nbr_element - swap_op->nbr_ru;
	swap_op->nbr_rrd = dest->nbr_element - swap_op->nbr_rd;
	get_optimum_plan(swap_op);
	set_nbr_swap(swap_op, src, i_src, type);
	return (swap_op);
}

t_operation	*get_cheapest_op(t_stack *src, t_stack *dest,
			t_sort_type type)
{
	size_t		i;
	t_operation	*r;
	t_operation	*tmp;

	i = -1;
	r = (t_operation *)malloc(sizeof(t_operation));
	if (r == NULL)
		error("[get_cheapest_op] Failed to allocate memory for struct");
	r->nbr_operation = -1;
	while (++i < src->nbr_element)
	{
		if (i > r->nbr_operation && (src->nbr_element - i) >= r->nbr_operation)
			continue ;
		tmp = calculate_optimum_plan(src, i, dest, type);
		if (tmp == NULL)
			error("[get_cheapest_op] Failed to allocate memory for struct");
		if (tmp->nbr_operation < r->nbr_operation)
		{
			free(r);
			r = tmp;
		}
		else
			free(tmp);
	}
	return (r);
}
