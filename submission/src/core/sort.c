/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 21:35:57 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/17 12:04:45 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void	sort_three_element(t_stacks *stacks)
{
	size_t	i_min;
	size_t	i_max;

	if (stacks->a->nbr_element != 3)
		return ;
	min(stacks->a->content, stacks->a->nbr_element, &i_min);
	max(stacks->a->content, stacks->a->nbr_element, &i_max);
	if (i_min == 0 && i_max == 2)
		return ;
	if (i_min == 0 && i_max == 1)
	{
		do_op(stacks, "rra", 1);
		do_op(stacks, "sa", 1);
	}
	else if (i_min == 1 && i_max == 0)
		do_op(stacks, "ra", 1);
	else if (i_min == 1 && i_max == 2)
		do_op(stacks, "sa", 1);
	else if (i_min == 2 && i_max == 0)
	{
		do_op(stacks, "ra", 1);
		do_op(stacks, "sa", 1);
	}
	else if (i_min == 2 && i_max == 1)
		do_op(stacks, "rra", 1);
}

void	sort_two_elements(t_stacks *stacks)
{
	if (stacks->a->nbr_element > 3 || stacks->b->nbr_element > 0)
		return ;
	if (stacks->a->nbr_element == 1)
		return ;
	if (stacks->a->nbr_element == 3)
		sort_three_element(stacks);
	else if (stacks->a->content[0] > stacks->a->content[1])
		do_op(stacks, "sa", 1);
}

static void	rotate_stack_final(t_stacks *stacks)
{
	size_t	i_min;
	size_t	n;

	min(stacks->a->content, stacks->a->nbr_element, &i_min);
	n = stacks->a->nbr_element - i_min;
	if (i_min <= stacks->a->nbr_element / 2)
	{
		while (i_min-- > 0)
			do_op(stacks, "ra", 1);
	}
	else
	{
		while (n-- > 0)
			do_op(stacks, "rra", 1);
	}
}

void	sort_stacks(t_stacks *stacks)
{
	t_operation	*op;
	t_operation	context;

	sort_two_elements(stacks);
	if (is_sorted(stacks) == 1)
		return ;
	do_op(stacks, "pb", 1);
	do_op(stacks, "pb", 1);
	set_op_context(&context, ATOB);
	while (stacks->a->nbr_element > 3)
	{
		op = get_cheapest_op(stacks->a, stacks->b, DESCENDING);
		commit_op(stacks, op, &context);
		free(op);
	}
	sort_three_element(stacks);
	set_op_context(&context, BTOA);
	while (stacks->b->nbr_element > 0)
	{
		op = get_cheapest_op(stacks->b, stacks->a, ASCENDING);
		commit_op(stacks, op, &context);
		free(op);
	}
	rotate_stack_final(stacks);
}
