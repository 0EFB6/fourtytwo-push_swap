/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 15:23:03 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/17 12:06:08 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "../push_swap.h"

static char	valid_str(const char *str)
{
	while (*str != '\0')
	{
		if (*str != '+' && *str != '-' && *str != ' ' && !(ft_isdigit(*str)))
			return (0);
		str++;
	}
	return (1);
}

static char	**split_argv(const char *str, char sep, size_t *n_ptr)
{
	char	**arr_str;
	size_t	nbr_ptr;

	arr_str = ft_split(str, sep);
	nbr_ptr = get_nbr_ptr((void **)arr_str);
	if (n_ptr != NULL)
		*n_ptr = nbr_ptr;
	if (n_ptr == 0)
		error("[split_argv] Empty instructions");
	return (arr_str);
}

static void	parse_argv(t_stack *stack, char **argv)
{
	char	**arr_str;
	size_t	nbr_ptr;
	size_t	i;

	i = 0;
	while (*argv != NULL)
	{
		arr_str = split_argv(*argv, ' ', &nbr_ptr);
		if (nbr_ptr != 1)
			stack->content = (int *)resize_arr(stack->content,
					stack->nbr_element, stack->nbr_element + nbr_ptr,
					sizeof(int));
		if (stack->content == NULL)
			error("[parse_argv] Failed to expand array");
		stack->nbr_element += nbr_ptr - 1;
		while (*arr_str != NULL)
		{
			if (!valid_str(*arr_str))
				error("[parse_argv] Invalid input");
			stack->content[i++] = ft_atoi_safe(*arr_str);
			arr_str++;
		}
		free_ptr_arr((void **) arr_str - nbr_ptr);
		argv++;
	}
}

t_stack	*parser(int argc, char **argv)
{
	t_stack	*stack;

	if (argc < 2)
		error("[parser] Incorrect number of arguments.");
	stack = init_stack((size_t)argc - 1);
	if (ft_strlen(*(argv + 1)) == 0 && *(argv + 2) == 0)
		error("[parser] Invalid input");
	parse_argv(stack, argv + 1);
	return (stack);
}

void	set_op_context(t_operation *context, t_push_type type)
{
	if (!(type == ATOB || type == BTOA))
		error("[set_op_context] Invalid argument: type");
	if (type == ATOB)
	{
		ft_memcpy(context->ru, "ra", 3);
		ft_memcpy(context->rd, "rb", 3);
		ft_memcpy(context->rru, "rra", 4);
		ft_memcpy(context->rrd, "rrb", 4);
		ft_memcpy(context->sw, "sa", 3);
	}
	else
	{
		ft_memcpy(context->ru, "rb", 3);
		ft_memcpy(context->rd, "ra", 3);
		ft_memcpy(context->rru, "rrb", 4);
		ft_memcpy(context->rrd, "rra", 4);
		ft_memcpy(context->sw, "sb", 3);
	}
}
