/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 13:37:57 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/17 12:13:04 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "../push_swap.h"

t_stack	*init_stack(size_t nbr_element)
{
	t_stack	*stack;

	stack = (t_stack *)malloc(sizeof(t_stack));
	if (stack == NULL)
		error("[init_stack] Failed to allocate memory for 'struct s_stack'");
	stack->content = (int *)ft_calloc(nbr_element, sizeof(int));
	if (stack->content == NULL)
		error("[init_stack] Failed to allocate memory for 'array content'");
	stack->nbr_element = nbr_element;
	return (stack);
}

t_stacks	*init_stacks(int argc, char **argv)
{
	t_stacks	*stack;

	stack = (t_stacks *)malloc(sizeof(t_stacks));
	if (stack == NULL)
		error("[init_stacks] Failed to allocate memory for struct s_stacks");
	stack->a = parser(argc, argv);
	stack->b = init_stack(stack->a->nbr_element);
	stack->b->nbr_element = 0;
	stack->counter = 0;
	return (stack);
}

void	destroy_stack(t_stack *stack)
{
	free(stack->content);
	free(stack);
}

void	destroy_stacks(t_stacks *stacks)
{
	destroy_stack(stacks->a);
	destroy_stack(stacks->b);
	free(stacks);
}
