/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 14:54:23 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/16 17:02:25 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "../push_swap.h"

void	*resize_arr(void *arr, size_t old_size, size_t new_size, size_t size)
{
	void	*new_arr;

	if (new_size == 0 || size == 0)
		return (NULL);
	new_arr = ft_calloc(new_size, size);
	if (!new_arr)
		return (NULL);
	if (!arr)
		return (new_arr);
	if (old_size > new_size)
		new_arr = ft_memcpy(new_arr, arr, new_size * size);
	else
		new_arr = ft_memcpy(new_arr, arr, old_size * size);
	free(arr);
	return (new_arr);
}

void	free_ptr_arr(void **ptr_arr)
{
	size_t	i;

	i = 0;
	while (ptr_arr[i] != NULL)
	{
		free(ptr_arr[i]);
		i++;
	}
	free(ptr_arr);
}

size_t	get_nbr_ptr(void **ptr_arr)
{
	size_t	i;

	i = 0;
	while (ptr_arr[i] != NULL)
		i++;
	return (i);
}

size_t	get_element_index(int n, int *arr, size_t len)
{
	size_t	i;

	i = 0;
	if (arr == NULL)
		return (-1);
	while (i < len)
	{
		if (arr[i] == n)
			return (i);
		i++;
	}
	return (-1);
}
