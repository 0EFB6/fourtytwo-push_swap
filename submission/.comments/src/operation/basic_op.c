/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   basic_op.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/03 18:53:31 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/05 21:17:58 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "../push_swap.h"

/*
** Find the index ar which integer n should be inserted into array for array to
** remain sorted. The type can be either ascending or descending, as stated in
** header file.
** Integer n can be placed before the first element and after the last element
** if such placement results in the minimum difference. The return value would
** be 0 in that case, which signifies that n should be inserted as the first 
** element in the array.
** Return the index at which n should be inserted.
*/
size_t	insert(int n, const int *arr, size_t len, t_sort_type type)
{
	size_t	i;
	size_t	i_terminate;

	if (arr == NULL)
		error("[insert] Invalid argument: arr");
	if (!(type == ASCENDING || type == DESCENDING))
		error("[insert] Invalid argument: sort_type");
	if (type == ASCENDING)
		min(arr, len, &i);
	else
		max(arr, len, &i);
	i_terminate = i;
	while (1)
	{
		if ((type == ASCENDING && n < arr[i])
			|| (type == DESCENDING && n > arr[i]))
			return (i);
		i = roll_index(i, 1, 0, len - 1);
		if (i == i_terminate)
			break ;
	}
	return (i);
}

/*
** Function to push the first element from source stack to destination stack
** Update the number of elements inside both stack
*/
char	push(t_stack *dest, t_stack *src)
{
	if (dest == NULL || src == NULL)
		return (-1);
	if (src->nbr_element == 0)
		return (0);
	ft_memmove(dest->content + 1, dest->content,
		dest->nbr_element * sizeof(*dest->content));
	dest->content[0] = src->content[0];
	dest->nbr_element += 1;
	src->nbr_element -= 1;
	ft_memmove(src->content, src->content + 1,
		src->nbr_element * sizeof(*src->content));
	return (0);
}

/*
** Similar to ra, rb. Shift up all elements in a stack by 1, so the first
** element becomes the last one in the stack
*/
char	rotate_up(t_stack *stack)
{
	int	tmp;

	if (stack == NULL)
		return (-1);
	if (stack->nbr_element < 2)
		return (0);
	tmp = stack->content[0];
	ft_memmove(stack->content, stack->content + 1,
		(stack->nbr_element - 1) * sizeof(*stack->content));
	stack->content[stack->nbr_element - 1] = tmp;
	return (0);
}

/*
** Similar to rra, rrb. Shift down all elements in a stack by 1, so the last
** element becomes the first one in the stack
*/
char	rotate_down(t_stack *stack)
{
	int	tmp;

	if (stack == NULL)
		return (-1);
	if (stack->nbr_element < 2)
		return (0);
	tmp = stack->content[stack->nbr_element - 1];
	ft_memmove(stack->content + 1, stack->content,
		(stack->nbr_element - 1) * sizeof(*stack->content));
	stack->content[0] = tmp;
	return (0);
}

/*
** Swapping the first two elements in a stack
*/
char	swap(t_stack *stack)
{
	int	tmp;

	if (stack == NULL || stack->content == NULL)
		return (-1);
	if (stack->nbr_element < 2)
		return (0);
	tmp = stack->content[0];
	stack->content[0] = stack->content[1];
	stack->content[1] = tmp;
	return (0);
}
