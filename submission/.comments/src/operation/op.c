/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/03 13:06:39 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/17 11:38:36 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "../push_swap.h"

static void	set_nbr_swap(t_operation *swap_op, t_stack *src, size_t i_src,
	t_sort_type type)
{
	int	previous_element;
	int	element_to_push;

	swap_op->nbr_sw = 0;
	if ((swap_op->op_type != RR_RURD_SW && swap_op->op_type != RU_SW_RRD)
		|| (swap_op->op_type == RR_RURD_SW
			&& swap_op->nbr_rd >= swap_op->nbr_ru)
		|| (swap_op->op_type == RU_SW_RRD && swap_op->nbr_ru == 0))
		return ;
	element_to_push = src->content[i_src];
	if (i_src > 0)
		previous_element = src->content[i_src];
	else
		previous_element = src->content[src->nbr_element - 1];
	if ((type == DESCENDING && element_to_push < previous_element)
		|| (type == ASCENDING && element_to_push > previous_element))
	{
		swap_op->nbr_sw++;
		swap_op->nbr_ru--;
	}
}

static void	set_plan(t_operation *swap_op, size_t nbr_operation,
	t_op_type op_type)
{
	swap_op->nbr_operation = nbr_operation;
	swap_op->op_type = op_type;
}

/*
** Sets swap_op->nbr_operation and swap_op->op_type to reflect the information
** about the optimum set of operations, based on values of the following
** members: nbr_ru, nbr_rru, nbr_rd, nbr_rrd
*/
static void	get_optimum_plan(t_operation *swap_op)
{
	size_t	nbr_op;

	swap_op->nbr_operation = swap_op->nbr_ru;
	if (swap_op->nbr_rd > swap_op->nbr_operation)
		swap_op->nbr_operation = swap_op->nbr_rd;
	swap_op->op_type = RR_RURD_SW;
	nbr_op = swap_op->nbr_rru + swap_op->nbr_rd;
	if (nbr_op < swap_op->nbr_operation)
		set_plan(swap_op, nbr_op, RRU_RD);
	nbr_op = swap_op->nbr_ru + swap_op->nbr_rrd;
	if (nbr_op < swap_op->nbr_operation)
		set_plan(swap_op, nbr_op, RU_SW_RRD);
	nbr_op = swap_op->nbr_rru;
	if (nbr_op < swap_op->nbr_rrd)
		nbr_op = swap_op->nbr_rrd;
	if (nbr_op < swap_op->nbr_operation)
		set_plan(swap_op, nbr_op, RRR_RRURRD);
}

/*
** Calculate the optimum set of operations needed to bring the element at
** i_src in src stack to the correct position in dest stack
*/
static t_operation	*calculate_optimum_plan(t_stack *src, size_t i_src,
				t_stack *dest, t_sort_type type)
{
	t_operation	*swap_op;

	swap_op = (t_operation *)malloc(sizeof(t_operation));
	if (swap_op == NULL)
		return (NULL);
	swap_op->nbr_ru = i_src;
	swap_op->nbr_rd = insert(src->content[i_src], dest->content,
			dest->nbr_element, type);
	swap_op->nbr_rru = src->nbr_element - swap_op->nbr_ru;
	swap_op->nbr_rrd = dest->nbr_element - swap_op->nbr_rd;
	if (DEBUG)
		ft_printf("OP:%d\nRU:%d\nRD:%d\nRRU:%d\nRRD:%d\nSW:%d\n---------\n", swap_op->nbr_operation, swap_op->nbr_ru, swap_op->nbr_rd, swap_op->nbr_rru, swap_op->nbr_rrd, swap_op->nbr_sw);
	get_optimum_plan(swap_op);
	set_nbr_swap(swap_op, src, i_src, type);
	if (DEBUG)
		ft_printf("\nNEW:\nOP:%d\nRU:%d\nRD:%d\nRRU:%d\nRRD:%d\nSW:%d\n---------\n", swap_op->nbr_operation, swap_op->nbr_ru, swap_op->nbr_rd, swap_op->nbr_rru, swap_op->nbr_rrd, swap_op->nbr_sw);
	return (swap_op);
}

/*
** Returns the cheapest set of operations to bring an element from src to dest
*/
t_operation	*get_cheapest_op(t_stack *src, t_stack *dest,
			t_sort_type type)
{
	size_t		i;
	t_operation	*r;
	t_operation	*tmp;

	i = -1;
	r = (t_operation *)malloc(sizeof(t_operation));
	if (r == NULL)
		error("Failed to allocate memory for struct");
	r->nbr_operation = -1;
	while (++i < src->nbr_element)
	{
		//ft_printf("\nHEOL:%d %d %d %d\n", i, r->nbr_operation, (src->nbr_element - i), r->nbr_operation);
		if (i > r->nbr_operation && (src->nbr_element - i) >= r->nbr_operation)
		{
			//ft_printf("HELLO%d", i);
			continue ;
		}
		tmp = calculate_optimum_plan(src, i, dest, type);
		if (tmp == NULL)
			error("Failed to allocate memory for struct");
		if (tmp->nbr_operation < r->nbr_operation)
		{
			ft_printf("\nHOLA:%d %d\n", tmp->nbr_operation, r->nbr_operation);
			free(r);
			r = tmp;
		}
		else
			free(tmp);
	}
	return (r);
}
