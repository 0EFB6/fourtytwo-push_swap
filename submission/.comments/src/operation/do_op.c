/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_op.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 20:25:38 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/16 16:44:59 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "../push_swap.h"

/*
** Do operation sa, sb or ss
** For ss, use bitwose operation since swap() may return negative value
** Return -1 by default
*/
static char	do_op_swap(t_stacks *stacks, const char *op)
{
	char	error;

	error = 0;
	if (!ft_strncmp(op, "sa", 3))
		return (swap(stacks->a));
	else if (!ft_strncmp(op, "sb", 3))
		return (swap(stacks->b));
	else if (!ft_strncmp(op, "ss", 3))
	{
		error |= swap(stacks->a);
		error |= swap(stacks->b);
		return (error);
	}
	return (-1);
}

/*
** Do operation ra, rb or rr
** For rr, use bitwise operation since rotate_up() may return negative value
** Return -1 by default
*/
static char	do_op_rotate_up(t_stacks *stacks, const char *op)
{
	char	error;

	error = 0;
	if (!ft_strncmp(op, "ra", 3))
		return (rotate_up(stacks->a));
	else if (!ft_strncmp(op, "rb", 3))
		return (rotate_up(stacks->b));
	else if (!ft_strncmp(op, "rr", 3))
	{
		error |= rotate_up(stacks->a);
		error |= rotate_up(stacks->b);
		return (error);
	}
	return (-1);
}

/*
** Do operation rra, rrb or rrr
** For rr, use bitwise operation since rotate_down() may return negative value
** Return -1 by default
*/
static char	do_op_rotate_down(t_stacks *stacks, const char *op)
{
	char	error;

	error = 0;
	if (!ft_strncmp(op, "rra", 4))
		return (rotate_down(stacks->a));
	else if (!ft_strncmp(op, "rrb", 4))
		return (rotate_down(stacks->b));
	else if (!ft_strncmp(op, "rrr", 4))
	{
		error |= rotate_down(stacks->a);
		error |= rotate_down(stacks->b);
		return (error);
	}
	return (-1);
}

/*
** Do operation pa or pb
** Return -1 by default
*/
static char	do_op_push(t_stacks *stacks, const char *op)
{
	if (!ft_strncmp(op, "pa", 3))
		return (push(stacks->a, stacks->b));
	else if (!ft_strncmp(op, "pb", 3))
		return (push(stacks->b, stacks->a));
	return (-1);
}

/*
** Check the (char *)op and do respective operations
** Print out the operation each time when one operation is executed
*/
void	do_op(t_stacks *stacks, const char *op, char printable_op)
{
	char	success;

	success = 0;
	if (stacks == NULL || op == NULL || ft_strlen(op) < 2)
		return ;
	if (*op == 's')
		success |= !do_op_swap(stacks, op);
	else if (*op == 'p')
		success |= !do_op_push(stacks, op);
	else if (*op == 'r')
	{
		if (ft_strlen(op) == 2)
			success |= !do_op_rotate_up(stacks, op);
		else
			success |= !do_op_rotate_down(stacks, op);
	}
	if (!success)
	{
		if (HELPFUL_ERROR_MSG)
			ft_putendl_fd("Failed to complete operation: ", STDERR_FILENO);
		error(op);
	}
	if (printable_op)
		ft_putendl_fd((char *)op, STDOUT_FILENO);
	stacks->counter++;
}
