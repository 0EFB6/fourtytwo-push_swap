/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 13:56:08 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/16 15:38:33 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "../push_swap.h"

int	max_int(void)
{
	return (((unsigned int) - 1) >> 1);
}

int	min_int(void)
{
	int	i;
	int	j;

	i = 1 << ((sizeof(int) * 8) - 1);
	ft_memset(&j, 0xff, sizeof(int));
	if (i < j)
		return (i);
	return (j);
}

/*
** Returns the value of the minimum element in the array (or min_int).
** If i_min is not NULL, it is set to be equal to the index of the minimum
** element.
*/
int	min(const int *arr, size_t len, size_t *i_min)
{
	int		min;
	size_t	i;

	min = max_int();
	i = 0;
	if (arr == NULL)
		return (0);
	while (i < len)
	{
		if (arr[i] <= min)
		{
			min = arr[i];
			if (i_min != NULL)
				*i_min = i;
		}
		i++;
	}
	return (min);
}

/*
** Return the value of the maximum element in the array (or max_int).
** If i_max is not NULL, it is set to be equal to the index of the maximum
** element.
*/
int	max(const int *arr, size_t len, size_t *i_max)
{
	int		max;
	size_t	i;

	max = min_int();
	i = 0;
	if (arr == NULL)
		return (0);
	while (i < len)
	{
		if (arr[i] >= max)
		{
			max = arr[i];
			if (i_max != NULL)
				*i_max = i;
		}
		i++;
	}
	return (max);
}

/*
** Returns index incremented by diff if the result is less than max and
** SIZE_T_MAX.
** Return min otherwise.
*/
size_t	roll_index(size_t index, size_t diff, size_t min, size_t max)
{
	if (((size_t) - 1) - index < diff)
		return (min);
	else if (index + diff <= max)
		return (index + diff);
	else
		return (min);
}
