/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_error.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 11:35:20 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/03 22:21:22 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "../push_swap.h"

/*
** Wrtie message to fd ending with new lines ('\n')
*/
void	ft_putendl_fd(char *str, int fd)
{
	if (str)
	{
		write(fd, str, ft_strlen(str));
		write(fd, "\n", 1);
	}
}

/*
** Wrtie error message to STDERR_FILENO, check is there is helpful message
** if there isn't, just write "Error"
*/
void	error(const char *msg)
{
	if (HELPFUL_ERROR_MSG)
		ft_putendl_fd((char *)msg, STDERR_FILENO);
	else
		ft_putendl_fd("Error", STDERR_FILENO);
	exit(-1);
}
