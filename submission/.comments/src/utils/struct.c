/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 13:37:57 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/16 15:45:41 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "../push_swap.h"

/*
** Allocate memory for a single stack
*/
t_stack	*init_stack(size_t nbr_element)
{
	t_stack	*stack;

	stack = (t_stack *)malloc(sizeof(t_stack));
	if (stack == NULL)
		error("Failed to allocate memory for struct s_stack");
	stack->content = (int *)ft_calloc(nbr_element, sizeof(int));
	if (stack->content == NULL)
		error("Failed to allocate memory for integer array content");
	stack->nbr_element = nbr_element;
	return (stack);
}

/*
** Initialized two stacks for stack A and stack B
*/
t_stacks	*init_stacks(int argc, char **argv)
{
	t_stacks	*stack;

	stack = (t_stacks *)malloc(sizeof(t_stacks));
	if (stack == NULL)
		error("Failed to allocate memory for struct s_stacks");
	stack->a = parser(argc, argv);
	stack->b = init_stack(stack->a->nbr_element);
	stack->b->nbr_element = 0;
	stack->counter = 0;
	return (stack);
}

/*
** Freeing the content of the stacks which is an array of integers
** Freeing the stacks
*/
void	destroy_stack(t_stack *stack)
{
	free(stack->content);
	free(stack);
}

/*
** Destroy the two stacks initialized in the beginning
*/
void	destroy_stacks(t_stacks *stacks)
{
	destroy_stack(stacks->a);
	destroy_stack(stacks->b);
	free(stacks);
}
