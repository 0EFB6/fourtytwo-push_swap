/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 15:23:03 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/16 15:45:19 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "../push_swap.h"

/*
** Validate string, return false if it A-Z & a-z as it is not a digit and also
** not '+', '-' and ' '
** Return true if is digit
*/
static char	valid_str(const char *str)
{
	while (*str != '\0')
	{
		if (*str != '+' && *str != '-' && *str != ' ' && !(ft_isdigit(*str)))
			return (0);
		str++;
	}
	return (1);
}

/*
** Spliting the array of string based on the separator passed as argument
** Set the argument count (number of integers) to n_ptr
** Return the array of string(numbers) 
*/
static char	**split_argv(const char *str, char sep, size_t *n_ptr)
{
	char	**arr_str;
	size_t	nbr_ptr;

	arr_str = ft_split(str, sep);
	nbr_ptr = get_nbr_ptr((void **)arr_str);
	if (n_ptr != NULL)
		*n_ptr = nbr_ptr;
	if (n_ptr == 0)
		error("Empty instructions");
	return (arr_str);
}

/*
** Split the argv first, then, resize the array and assign the string
** (number) to stack A using ft_atoi safe
*/
static void	parse_argv(t_stack *stack, char **argv)
{
	char	**arr_str;
	size_t	nbr_ptr;
	size_t	i;

	i = 0;
	while (*argv != NULL)
	{
		arr_str = split_argv(*argv, ' ', &nbr_ptr);
		if (nbr_ptr != 1)
			stack->content = (int *)resize_arr(stack->content,
					stack->nbr_element, stack->nbr_element + nbr_ptr,
					sizeof(int));
		if (stack->content == NULL)
			error("Failed to expand array");
		stack->nbr_element += nbr_ptr - 1;
		while (*arr_str != NULL)
		{
			if (!valid_str(*arr_str))
				error("Invalid input");
			stack->content[i++] = ft_atoi_safe(*arr_str);
			arr_str++;
		}
		free_ptr_arr((void **) arr_str - nbr_ptr);
		argv++;
	}
}

/*
** Intitialize stack and flush all argv numbers into the stack while
** making sure no errors or duplication, if there is, prompt "Error"
*/
t_stack	*parser(int argc, char **argv)
{
	t_stack	*stack;

	if (argc < 2)
		error("Incorrect number of arguments.");
	stack = init_stack((size_t)argc - 1);
	if (ft_strlen(*(argv + 1)) == 0 && *(argv + 2) == 0)
		error("Invalid input");
	parse_argv(stack, argv + 1);
	return (stack);
}

/*
** [Context]
** Not in the parser section, just setting the operation context for
** sorting and committing operation
*/
void	set_operation_context(t_operation *context, t_push_type type)
{
	if (!(type == ATOB || type == BTOA))
		error("[set_operation_context] Invalid argument: type");
	if (type == ATOB)
	{
		ft_memcpy(context->ru, "ra", 3);
		ft_memcpy(context->rd, "rb", 3);
		ft_memcpy(context->rru, "rra", 4);
		ft_memcpy(context->rrd, "rrb", 4);
		ft_memcpy(context->sw, "sa", 3);
	}
	else
	{
		ft_memcpy(context->ru, "rb", 3);
		ft_memcpy(context->rd, "ra", 3);
		ft_memcpy(context->rru, "rrb", 4);
		ft_memcpy(context->rrd, "rra", 4);
		ft_memcpy(context->sw, "sb", 3);
	}
}
