/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/31 14:46:41 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/04 11:29:34 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_cleanup(void *var, void *ret)
{
	free(var);
	return (ret);
}

char	*ft_get_current_line(char *buf)
{
	int		i;
	char	*newstr;

	i = 0;
	if (!buf[i])
		return (NULL);
	while (buf[i] && buf[i] != '\n')
		i++;
	newstr = ft_calloc(i + 2, sizeof(char));
	i = 0;
	while (buf[i] && buf[i] != '\n')
	{
		newstr[i] = buf[i];
		i++;
	}
	if (buf[i] && buf[i] == '\n')
		newstr[i++] = '\n';
	return (newstr);
}

char	*ft_remove_current_line(char *buf)
{
	int		i;
	int		j;
	char	*newstr;

	i = 0;
	j = 0;
	while (buf[i] && buf[i] != '\n')
		i++;
	if (!buf[i])
		return (ft_cleanup(buf, NULL));
	newstr = ft_calloc((ft_strlen(buf) - i + 1), sizeof(char));
	i++;
	while (buf[i])
		newstr[j++] = buf[i++];
	return (ft_cleanup(buf, newstr));
}

char	*ft_read(int fd, char *staticbuffer)
{
	int		readbyte;
	char	*buf;
	char	*tmp;

	if (!staticbuffer)
		staticbuffer = ft_calloc(1, 1);
	readbyte = 1;
	buf = ft_calloc(BUFFER_SIZE + 1, sizeof(char));
	while (readbyte > 0)
	{
		readbyte = read(fd, buf, BUFFER_SIZE);
		if (readbyte == -1)
			return (ft_cleanup(buf, NULL));
		buf[readbyte] = 0;
		tmp = ft_strjoin(staticbuffer, buf);
		free(staticbuffer);
		staticbuffer = tmp;
		if (ft_strchr(buf, '\n'))
			break ;
	}
	return (ft_cleanup(buf, staticbuffer));
}

char	*get_next_line(int fd)
{
	char		*ret;
	static char	*staticbuffer[1024];

	if (fd < 0 || BUFFER_SIZE <= 0 || read(fd, 0, 0) < 0)
		return (NULL);
	staticbuffer[fd] = ft_read(fd, staticbuffer[fd]);
	if (!staticbuffer[fd])
		return (NULL);
	ret = ft_get_current_line(staticbuffer[fd]);
	staticbuffer[fd] = ft_remove_current_line(staticbuffer[fd]);
	return (ret);
}
