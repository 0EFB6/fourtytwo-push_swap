/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils_len_print.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 15:08:35 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/29 00:16:04 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

// Calcilate the length of the string and return the length
// If the string is empty, return 1
int	ft_len(const char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
		i++;
	if (i == 0)
		return (1);
	return (i);
}

// Write out characters to standard output recursively based on the number n
// Return the number of characters written out
int	ft_putnchar_fd(char c, int fd, int n)
{
	int	i;

	i = 0;
	while (n-- > 0)
		i += (int)write(fd, &c, 1);
	return (i);
}

// Write out characters to standard output recursively based on the number n
// which determines how many characters to be written out
// n is set to 1 if it is 0 to avoid printing out nothing
// Normally, the n is passed as the width of the string
int	ft_print_char(char *str, int n)
{
	int	i;

	i = 0;
	if (n == 0)
		n = 1;
	while (i < n)
	{
		write(1, &str[i], 1);
		i++;
	}
	return (i);
}

// Write out the string to standard output
// If the specifier is 's' and the dot flag is true and the precision is 0,
// print out empty spaces based on the width or null.
// If the string is empty, print out empty spaces based on the width or null.
// Return the number of printed characters and stop the function.
//
// Initialized i with the length of string using ft_strlen()
// If the width is larger than 0 and the length of string is 0
// Print out the width of characters (null characters) and return the number of
// printed characters to i
// Else, print out the string to standard output.
// Finally, return the number of printed characters.
int	ft_print_str(char *str, t_format *f)
{
	int	i;

	if ((f->specifier == 's' && f->dot && f->precision == 0) || (str[0] == 0))
		return (ft_print_empty(str, f));
	i = ft_strlen(str);
	if (f->width > 0 && i == 0)
		i = ft_print_char(str, f->width);
	else
		ft_putstr_fd(str, 1);
	return (i);
}

// If the string is empty and the width is not 0, print out empty spaces and
// assign the number of printed characters to i
// Else if the string is empty and the dot flag is true and the precision is
// less than 0, print out null and assign the number of printed characters to i
// Else if the length of string is larger than 0 and the width is larger than 0,
// print out empty spaces based on the width
// Finally, return the number of printed characters.
int	ft_print_empty(char *str, t_format *f)
{
	int	i;

	i = 0;
	if (str[0] == 0 && f->width)
		i = ft_print_char(" ", 1);
	else if (str[0] == 0 && f->dot && f->precision < 0)
		i = ft_print_char(S_EMPTY, S_EMPTY_LEN);
	else if (ft_strlen(str) > 0 && f->width > 0)
	{
		while (i++ < f->width)
			ft_putchar_fd(' ', 1);
		i--;
	}
	return (i);
}
