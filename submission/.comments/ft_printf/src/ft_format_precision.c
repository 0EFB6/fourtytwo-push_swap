/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format_precision.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 16:49:41 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/29 13:34:44 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

// Set precision for string
// If dot flag is set and precision is 0, or IS_LINUX is set and string is
// same as "(null)" and precision is less than 6.
// allocate meomry for the return string using ft_calloc() with size of 1
// Else, get the string using ft_substr() starting from index 0 with length
// of precision and assign the return value to ret.
// Check if return string is valid, if it is not, return NULL
// Finally, free string and return the return string
static char	*ft_precision_string(char *str, t_format *f)
{
	char	*ret;

	if ((f->dot && f->precision == 0)
		|| (IS_LINUX
			&& (ft_strncmp(str, S_EMPTY, S_EMPTY_LEN) == 0
				&& f->precision < 6)))
		ret = ft_calloc(sizeof(char), 1);
	else
		ret = ft_substr(str, 0, f->precision);
	MALLOC_NULL_CHECK(ret);
	free(str);
	return (ret);
}

// Helper for ft_precision_number() and ft_precision_number_helper()
// Initialize i and j to 0, and len to the length of string
// If string is negative, increment i and j by 1, decrement len by 1 and
// increment precision by 1
// While i is less than precision, if i is less than precision - len,
// assign '0' to ret[i], else assign str[j] to ret[i] and increment j by 1
// Increment i by 1
// Finally, return ret
static char	*ft_precision_number_fill(char *str, char *ret, t_format *f)
{
	int	i;
	int	j;
	int	len;

	i = 0;
	j = 0;
	len = ft_strlen(str);
	if (str[0] == '-')
	{
		i++;
		j++;
		len--;
		f->precision++;
	}
	while (i < f->precision)
	{
		if (i < f->precision - len)
			ret[i] = '0';
		else
			ret[i] = str[j++];
		i++;
	}
	return (ret);
}

// Helper for ft_precision_number()
// If string is negative, allocate memory for the return string with size of
// precision + 2 using ft_calloc() and assign the return value to ret, check
// if the allocation is successful, if it is not, return NULL
// Assign '-' to the first index of ret
// Else, allocate memory for the return string with size of precision + 1
// using ft_calloc() and assign the return value to ret, check if the
// allocation is successful, if it is not, return NULL
// After that, call ft_precision_number_fill() to fill the string with '0' or
// the number
// Finally, return ret
static char	*ft_precision_number_helper(char *str, t_format *f)
{
	char	*ret;

	if (str[0] == '-')
	{
		ret = ft_calloc(sizeof(char), f->precision + 2);
		MALLOC_NULL_CHECK(ret);
		ret[0] = '-';
	}
	else
	{
		ret = ft_calloc(sizeof(char), f->precision + 1);
		MALLOC_NULL_CHECK(ret);
	}
	ft_precision_number_fill(str, ret, f);
	return (ret);
}

// Set precision for number
// If string is "0" and precision is 0, allocate memory for the return string
// with size of precision + 1 using ft_calloc() and assign the return value
// to ret, check if the allocation is successful, if it is not, return NULL,
// free string and return the return string
// Else if precision is greater than or equal to the length of string,
// Call ft_precision_number_helper() to set the precision for number and
// assign the return value to ret, check if ret is valid, if it is not,
// return NULL, free string and return the return string
// Or else, return the string
static char	*ft_precision_number(char *str, t_format *f)
{
	char	*ret;

	if (str[0] == '0' && f->precision == 0)
	{
		ret = ft_calloc(sizeof(char), f->precision + 1);
		MALLOC_NULL_CHECK(ret);
		free (str);
		return (ret);
	}
	else if (f->precision >= (int)ft_strlen(str))
	{
		ret = ft_precision_number_helper(str, f);
		MALLOC_NULL_CHECK(ret);
		free(str);
		return (ret);
	}
	return (str);
}

// Set precision for string and number
// If dot flag is not set, return the string
// If specifier is string or char, set the precision for string
// using ft_precision_string() and assign the return value to ret
// Else, set the precision for number using ft_precision_number()
// and assign the return value to ret
// Check if ret is valid, if it is not, return NULL
// Finally, return ret
char	*ft_precision(char *str, t_format *f)
{
	char	*ret;

	if (!f->dot)
		return (str);
	if (f->specifier == 's' || f->specifier == 'c')
		ret = ft_precision_string(str, f);
	else
		ret = ft_precision_number(str, f);
	MALLOC_NULL_CHECK(ret);
	return (ret);
}
