/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format_width.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 16:32:04 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/29 13:03:52 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

// Fill the string with '0' or ' ' depending on the flag
// Return the index of the string
static int	ft_fill(char *str, int i, int len, t_format *f)
{
	while (i < f->width - len)
	{
		if (f->zero && (!f->dot || f->precision < 0))
			str[i] = '0';
		else
			str[i] = ' ';
		i++;
	}
	return (i);
}

// Align the string to the left
// Allocate memory for return string with a size of width + 1 (null terminate)
// Check if the allocation is successful, if it is not, return NULL
// If the length of the string is 1 and the dot flag is set and the precision
// is 0 and the string is '0' or null, set the first index of the return
// string to ' '
// Loop through when the index is less than the width
// If the index is less than the length of the string and the specifier is 's'
// and the first index of the string is null, set the index of the return string
// to ' '
// Else if the index is less than the length of the string, set the index of the
// return string to the relevant string
// Else, set the index of the return string to ' '
// Incremment the index, i.
// Return the return string
static char	*ft_align_left(char *str, int len, t_format *f)
{
	char	*ret;
	int		i;

	i = 0;
	ret = ft_calloc(sizeof(char), f->width + 1);
	MALLOC_NULL_CHECK(ret);
	if (len == 1 && (f->dot && f->precision == 0) && (*str == '0' || *str == 0))
		ret[i++] = ' ';
	while (i < f->width)
	{
		if (i < len && (f->specifier == 's' && *str == 0))
			ret[i] = ' ';
		else if (i < len)
			ret[i] = str[i];
		else
			ret[i] = ' ';
		i++;
	}
	return (ret);
}

// Align the string to the right
// Initialize i and j with 0
// Allocate memory for return string with a size of width + 1 (null terminate)
// Check if the allocation is successful, if it is not, return NULL
// If the first index of the string is '-' and zero flag is true and dot flag
// is false or precision is less than 0, set the return string to string based
// on their repective index, i and j. Increment width by 1.
// Fill the return string with '0' or ' ' depending on the flag and assign the
// teturn value to i using ft_fill().
// If the length of the string is 1 and the dot flag is set and the precision
// is 0 and the string is '0' or null, set the index of the return to ' '.
// While j is less than the length of the string, set the return string to the
// string based on their respective index, i and j.
// Return the return string
static char	*ft_align_right(char *str, int len, t_format *f)
{
	char	*ret;
	int		i;
	int		j;

	i = 0;
	j = 0;
	ret = ft_calloc(sizeof(char), f->width + 1);
	MALLOC_NULL_CHECK(ret);
	if (str[0] == '-' && (f->zero && (!f->dot || f->precision < 0)))
	{
		ret[i++] = str[j++];
		f->width++;
	}
	i = ft_fill(ret, i, len, f);
	if (len == 1 && (f->dot && f->precision == 0) && (*str == '0' || *str == 0))
		ret[i++] = ' ';
	while (j < len)
		ret[i++] = str[j++];
	return (ret);
}

// Calculate the length of string using ft_len() and assign it to len
// If the width is less than or equal to the length of the string, return string
// since no padding is needed.
// If the minus flag is set, align the string to the left using ft_align_left()
// Else, align the string to the right using ft_align_right()
// Check if the return string is valid, if it is not, return NULL
// Free the string.
// Return the aligned string
char	*ft_width(char *str, t_format *f)
{
	char	*ret;
	int		len;

	len = ft_len(str);
	if (f->width <= len)
		return (str);
	if (f->minus)
		ret = ft_align_left(str, len, f);
	else
		ret = ft_align_right(str, len, f);
	MALLOC_NULL_CHECK(ret);
	free(str);
	return (ret);
}
