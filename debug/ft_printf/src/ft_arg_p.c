/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arg_p.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 12:17:55 by cwei-she          #+#    #+#             */
/*   Updated: 2023/07/28 23:44:59 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

// If string is not valid or null, allocate memory for the return string with
// size of 6 (P_EMPTY_LEN) + 1 (null terminate). Check if the allocation is
// successful. Copy the string to the return string using ft_memcpy() then
// return it.
// Or else, allocate memory for the tmp string with a size of 3 (character
// and null). Check if the allocation is successful. Assign the character '0x'
// to the first two index of the return string.
// Allocate memory for the hex string with string size in hexadecimal which 
// can be calculated using ft_baselen() + 1 (null terminate).
// Check if the allocation is successful.
// Convert the number to hexadecimal using ft_itoh() and assign it to hex.
// Join the two strings, tmp and hex together using ft_strjoin(), and assign
// it to the return string.
// Free the hex and tmp string.
// Return the return string.
char	*ft_specifier_p(size_t str)
{
	char	*ret;
	char	*hex;
	char	*tmp;

	if (!str)
	{
		ret = ft_calloc(sizeof(char), P_EMPTY_LEN + 1);
		MALLOC_NULL_CHECK(ret);
		ret = ft_memcpy(ret, P_EMPTY, P_EMPTY_LEN);
		return (ret);
	}
	tmp = ft_calloc(sizeof(char), 3);
	MALLOC_NULL_CHECK(tmp);
	tmp[0] = '0';
	tmp[1] = 'x';
	hex = ft_calloc(sizeof(char), ft_baselen(str) + 1);
	MALLOC_NULL_CHECK(hex);
	hex = ft_itoh(str, 0, hex);
	ret = ft_strjoin(tmp, hex);
	free(hex);
	free(tmp);
	return (ret);
}
