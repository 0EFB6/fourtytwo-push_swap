/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 14:37:29 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/06 14:57:19 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "push_swap.h"

int	main(int argc, char **argv)
{
	t_stacks	*stacks;

	if (argc == 1)
		return (0);
	stacks = init_stacks(argc, argv);
	if (!valid_stack(stacks->a))
		error("Invalid stack A");
	sort_stacks(stacks);
	if (DEBUG)
		ft_printf("Number of operations: %d\n", stacks->counter);
	destroy_stacks(stacks);
	return (0);
}
