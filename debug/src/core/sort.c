/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 21:35:57 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/16 16:42:47 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "../push_swap.h"

/*
** Sorts stack A when it has 3 elements.
** Get the index of the min and max element.
** If the min is 0 and max is 2, this means the stack is already sorted, so do
** nothing.
*/
void	sort_three_element(t_stacks *stacks)
{
	size_t	i_min;
	size_t	i_max;

	if (stacks->a->nbr_element != 3)
		return ;
	min(stacks->a->content, stacks->a->nbr_element, &i_min);
	max(stacks->a->content, stacks->a->nbr_element, &i_max);
	if (i_min == 0 && i_max == 2)
		return ;
	if (i_min == 0 && i_max == 1)
	{
		do_op(stacks, "rra", 1);
		do_op(stacks, "sa", 1);
	}
	else if (i_min == 1 && i_max == 0)
		do_op(stacks, "ra", 1);
	else if (i_min == 1 && i_max == 2)
		do_op(stacks, "sa", 1);
	else if (i_min == 2 && i_max == 0)
	{
		do_op(stacks, "ra", 1);
		do_op(stacks, "sa", 1);
	}
	else if (i_min == 2 && i_max == 1)
		do_op(stacks, "rra", 1);
}

/*
** Sort stack A when it has 2 or 3 elements when stack B is empty
*/
void	sort_two_elements(t_stacks *stacks)
{
	if (stacks->a->nbr_element > 3 || stacks->b->nbr_element > 0)
		return ;
	if (stacks->a->nbr_element == 1)
		return ;
	if (stacks->a->nbr_element == 3)
		sort_three_element(stacks);
	else if (stacks->a->content[0] > stacks->a->content[1])
		do_op(stacks, "sa", 1);
}

/*
** Do final sort until the stack is sorted, only 'ra' and 'rra'
** is used here
*/
static void	rotate_stack_final(t_stacks *stacks)
{
	size_t	i_min;
	size_t	n;

	min(stacks->a->content, stacks->a->nbr_element, &i_min);
	n = stacks->a->nbr_element - i_min;
	if (i_min <= stacks->a->nbr_element / 2)
	{
		while (i_min-- > 0)
			do_op(stacks, "ra", 1);
	}
	else
	{
		while (n-- > 0)
			do_op(stacks, "rra", 1);
	}
}

/*
** Sort array with two elements, if it is not 2 elements, proceed to next part
** If the stack is sorted, return and exit.
** Or else, assume stack is more than 2 elements and is not sorted,
** push 2 numbers to stack B
** Set the operations context which manipulates the rotate and swap direction
** before the two main while loop.
** Then, two main part(while loop) is executed to sort the numbers.
** (1) Mainly rotate and swap then push the numbers to stack B
** (2) Finalization(using rotate operations) and push all numbers back to
**     stack A, I would say most of the numbers(85%++) is sorted at this time
**     being
** Finally, with the rotate_stack_final(), stack A will be fully sorted and
** stack B should be empty.
** Between the two while loop part, sort three elements, the function is
** placed here to sort stack A that has exactly three elements, then proceed
** to the second part of the while loop or exit as the stack is sorted
**
** For each while loop, get the chepest operation and commit the operation
** Free the operation then continue to loop again and again
*/
void	sort_stacks(t_stacks *stacks)
{
	t_operation	*op;
	t_operation	context;

	sort_two_elements(stacks);
	if (is_sorted(stacks) == 1)
		return ;
	do_op(stacks, "pb", 1);
	do_op(stacks, "pb", 1);
	set_operation_context(&context, ATOB);
	while (stacks->a->nbr_element > 3)
	{
		op = get_cheapest_op(stacks->a, stacks->b, DESCENDING);
		commit_op(stacks, op, &context);
		free(op);

		if (DEBUG)
		{
			size_t i = -1;
			while (++i < stacks->a->nbr_element)
				ft_printf("%i ", stacks->a->content[i]);
			i = -1;
			ft_printf("\n");
			while (++i < stacks->b->nbr_element)
				ft_printf("%i ", stacks->b->content[i]);
			ft_printf("\n---------\n");
			//ft_printf("OP:%d\nRU:%d\nRD:%d\nRRU:%d\nRRD:%d\nSW:%d\n---------\n", op->nbr_operation, op->nbr_ru, op->nbr_rd, op->nbr_rru, op->nbr_rrd, op->nbr_sw);
		}
	}
	ft_printf("\n\n ######### BORDER ########\n\n");
	sort_three_element(stacks);
	set_operation_context(&context, BTOA);
	while (stacks->b->nbr_element > 0)
	{
		op = get_cheapest_op(stacks->b, stacks->a, ASCENDING);
		commit_op(stacks, op, &context);
		free(op);

		if (DEBUG)
		{
			size_t i = -1;
			while (++i < stacks->a->nbr_element)
				ft_printf("%i ", stacks->a->content[i]);
			i = -1;
			ft_printf("\n");
			while (++i < stacks->b->nbr_element)
				ft_printf("%i ", stacks->b->content[i]);
			ft_printf("\n---------\n");
		}
	}
	rotate_stack_final(stacks);
}
