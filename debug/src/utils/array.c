/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_array.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42kl.edu.my>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 14:54:23 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/04 12:57:18 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "../push_swap.h"

/*
** Expands array from old_size * size to new_size * size.
** Array is freed in the process.
** If array is NULL, a new array of size new_size * size is created
*/
void	*resize_arr(void *arr, size_t old_size, size_t new_size, size_t size)
{
	void	*new_arr;

	if (new_size == 0 || size == 0)
		return (NULL);
	new_arr = ft_calloc(new_size, size);
	if (!new_arr)
		return (NULL);
	if (!arr)
		return (new_arr);
	if (old_size > new_size)
		new_arr = ft_memcpy(new_arr, arr, new_size * size);
	else
		new_arr = ft_memcpy(new_arr, arr, old_size * size);
	free(arr);
	return (new_arr);
}

/*
** Free each element in an array of pointers, then free the array itself
** Array must be null-terminated
*/
void	free_ptr_arr(void **ptr_arr)
{
	size_t	i;

	i = 0;
	while (ptr_arr[i] != NULL)
	{
		free(ptr_arr[i]);
		i++;
	}
	free(ptr_arr);
}

/*
** Get number of pointers in a null-terminated array
*/
size_t	get_nbr_ptr(void **ptr_arr)
{
	size_t	i;

	i = 0;
	while (ptr_arr[i] != NULL)
		i++;
	return (i);
}

/*
** Loop through the array and check whether n matches, if matches,
** return the index number, i. Else, return -1.
*/
size_t	get_element_index(int n, int *arr, size_t len)
{
	size_t	i;

	i = 0;
	if (arr == NULL)
		return (-1);
	while (i < len)
	{
		if (arr[i] == n)
			return (i);
		i++;
	}
	return (-1);
}
