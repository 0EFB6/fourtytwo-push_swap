/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   atoi.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwei-she <cwei-she@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 18:35:47 by cwei-she          #+#    #+#             */
/*   Updated: 2023/08/16 15:35:19 by cwei-she         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "../push_swap.h"

/*
** Return 1 if the character is space
*/
static int	is_space(char c)
{
	if (c == ' ' || c == '\t' || c == '\n' || \
			c == '\v' || c == '\f' || c == '\r')
		return (1);
	return (0);
}

/*
** Return 0 if the character is not sign
*/
static int	is_sign(char c)
{
	if (c == '-')
		return (-1);
	else if (c == '+')
		return (1);
	return (0);
}

/*
** Return 1 if is a valid integer which is in the range of
** -2147483648 and 2147483647
*/
static char	is_valid_int(long n)
{
	if (n > max_int() || n < min_int())
		return (0);
	return (1);
}

/*
** Safe version of atoi, producing error when encountered
** Return the number as integer
*/
int	ft_atoi_safe(const char *s)
{
	int		i;
	int		sign;
	long	res;

	i = 0;
	sign = 1;
	res = 0;
	while (is_space(s[i]))
		i++;
	if (is_sign(s[i]))
		sign *= is_sign(s[i++]);
	if (!ft_isdigit(s[i]))
		error("[ft_atoi_safe]Invalid string");
	while (ft_isdigit(s[i]))
	{
		res = res * 10 + (s[i++] - '0');
		if (!is_valid_int(res * sign))
			error("[ft_atoi_safe]Integer overflow");
	}
	if (s[i] != '\0')
		error("[ft_atoi_safe]Invalid string");
	return ((int)(res * sign));
}
